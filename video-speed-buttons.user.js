// ==UserScript==
// @name         Video Speed Buttons
// @description  Add speed buttons to any HTML5 <video> element. Comes with a loader for YouTube and Vimeo
// @namespace    bradenscode
// @version      1.0.8.1
// @copyright    2017, Braden Best
// @run-at       document-end
// @author       Braden Best
// @grant        none
//
// @match        *://*.youtube.com/*
// @match        *://*.vimeo.com/*
// ==/UserScript==

// To add a new site: add a @match above, and modify loader_data.container_candidates near the bottom

function video_speed_buttons(anchor, video_el){
    if(!anchor || !video_el){
        return null;
    }

    const COLOR_SELECTED = "black",
          COLOR_NORMAL = "grey",
          BUTTON_SIZE = "100%",
          DEFAULT_SPEED = 1.0,
          LABEL_TEXT = "Video Speed:   ",
          DEFAULT_BRIGHTNESS = 1.0,
          BRIGHT_LABEL_TEXT = "Brightness:    ",
          SEEK_LABEL_TEXT = "Seek:          ";

    const SPEED_BUTTON_TEMPLATES = [
        ["Normal", 1],
        ["1.25",   1.25],
        ["1.5",    1.5],
        ["1.75",   1.75],
        ["2",      2],
        ["2.5",    2.5],
        ["3x",     3],
        ["4x",     4]
    ];

    const BRIGHT_BUTTON_TEMPLATES = [
        ["Normal", 1.0],
        ["150%",   1.5],
        ["200%",   2.0],
        ["250%",   2.5],
        ["+10%",   0.1],
        ["-10%",  -0.1],
        ["+25%",   0.25],
        ["-25%",  -0.25]
    ];

    const SEEK_BUTTON_TEMPLATES = [
        ["-5s",  -5],
        ["5s",    5],
        ["+10s",  10],
        ["-10s", -10]
    ]

    const buttons = {
        head:      null,
        selected:  null,
        last:      null
    };

    const brightButtons = {
        head: null,
        selected: null,
        last: null,
    };

    /*
    const keyboard_controls = [
        ["-", "Speed Down", function(ev){
            if(is_comment_box(ev.target))
                return false;

            (buttons.selected || buttons.head)
                .getprev()
                .el
                .dispatchEvent(new MouseEvent("click"));
        }],
        ["+", "Speed Up", function(ev){
            if(is_comment_box(ev.target))
                return false;

            (buttons.selected || buttons.head)
                .getnext()
                .el
                .dispatchEvent(new MouseEvent("click"));
        }],
        ["*", "Reset Speed", function(ev){
            let selbtn = buttons.head;
            let result = null;

            if(is_comment_box(ev.target))
                return false;

            while(selbtn !== null && result === null)
                if(selbtn.speed === DEFAULT_SPEED)
                    result = selbtn;
                else
                    selbtn = selbtn.next;

            if(result === null)
                result = buttons.head;

            result.el.dispatchEvent(new MouseEvent("click"));
        }],
        ["?", "Show Help", function(ev){
            let infobox;

            if(is_comment_box(ev.target))
                return false;

            (infobox = Infobox(speedContainer))
                .log("Keyboard Controls (click to close)<br>");

            keyboard_controls.forEach(function([key, description]){
                infobox.log(`    [${key}]  ${description}<br>`);
            });
        }]
    ];
*/

    const brightContainer = (function(){
        let div = document.createElement("div");
        let prev_node = null;
        let self;

        div.className = "vbb-container";
        div.style.borderBottom = "1px solid #ccc";
        div.style.marginBottom = "10px";
        div.style.paddingBottom = "10px";
        div.appendChild(document.createTextNode(BRIGHT_LABEL_TEXT));

        let el = document.createElement("span");
        el.id = "brightnessCurrentValue";
        el.innerHTML = "100%";
        el.style.userSelect = "none";
        el.style.marginLeft = "20px";
        el.style.fontWeight = "bold";
        el.style.fontSize = BUTTON_SIZE;
        el.style.color = COLOR_SELECTED;

        BRIGHT_BUTTON_TEMPLATES.forEach(function(button){
            let brightButton = BrightnessButton(...button, div);

            if(brightButtons.head === null)
                brightButtons.head = brightButton;

            if(prev_node !== null){
                brightButton.prev = prev_node;
                prev_node.next = brightButton;
            }

            prev_node = brightButton;

            if(brightButton.brightness == DEFAULT_BRIGHTNESS)
                brightButton.select();
        });


        div.appendChild(el);
        return self = {
            div,
            el
        };
    })();

    const seekContainer = (function(){
        let div = document.createElement("div");
        let prev_node = null;
        let self;

        div.className = "vbsk-container";
        div.style.borderBottom = "1px solid #ccc";
        div.style.marginBottom = "10px";
        div.style.paddingBottom = "10px";
        div.appendChild(document.createTextNode(SEEK_LABEL_TEXT));

        SEEK_BUTTON_TEMPLATES.forEach(function(button){
            let seekButton = SeekButton(...button, div);
        });

        return div;
    })();

    function displayCurrentBrightness(){
        brightContainer.el.innerHTML = getBrightness(video_el);
    }

    const speedContainer = (function(){
        let div = document.createElement("div");
        let prev_node = null;

        div.className = "vsb-container";
        div.style.borderBottom = "1px solid #ccc";
        div.style.marginBottom = "10px";
        div.style.paddingBottom = "10px";
        div.appendChild(document.createTextNode(LABEL_TEXT));

        SPEED_BUTTON_TEMPLATES.forEach(function(button){
            let speedButton = SpeedButton(...button, div);

            if(buttons.head === null){
                buttons.head = speedButton;
            }

            if(prev_node !== null){
                speedButton.prev = prev_node;
                prev_node.next = speedButton;
            }

            prev_node = speedButton;

            if(speedButton.speed == DEFAULT_SPEED){
                speedButton.select();
            }
        });


        return div;
    })();

    function is_comment_box(el){
        const candidate = [
            ".comment-simplebox-text",
            "textarea"
        ].map(c => document.querySelector(c))
        .find(el => el !== null);

        if(candidate === null){
            logvsb("video_speed_buttons::is_comment_box", "no candidate for comment box. Assuming false.");
            return 0;
        }

        return el === candidate;
    }

    function Infobox(parent){
        let el = document.createElement("pre");

        el.style.font = "1em monospace";
        el.style.borderTop = "1px solid #ccc";
        el.style.marginTop = "10px";
        el.style.paddingTop = "10px";

        el.addEventListener("click", function(){
            parent.removeChild(el);
        });

        parent.appendChild(el);

        function log(msg){
            el.innerHTML += msg;
        }

        return {
            el,
            log
        };
    }

    function setPlaybackRate(el, rate){
        if(el)
            el.playbackRate = rate;
        else
            logvsb("video_speed_buttons::setPlaybackRate", "video element is null or undefined", 1);
    }

    function getBrightness(el){
        let curBrightValue = el.style.filter;
        let bMatches;
        if(curBrightValue != ""){
            bMatches = curBrightValue.match(/(.*\()(.*)(\))/);
            curBrightValue = (parseFloat(bMatches[2]) * 100).toFixed(0) + "%";
        } else
            curBrightValue = "100%";
        return curBrightValue;
    }

    function setBrightness(el, brightness){
        el.style.filter = "brightness(" + brightness.toFixed(2) + ")";
    }

    function changeBrightness(el, bdelta){
        let curBrightValue = el.style.filter;
        let bMatches;
        if(curBrightValue != ""){
            bMatches = curBrightValue.match(/(.*\()(.*)(\))/);
            curBrightValue = bMatches[1] + (parseFloat(bMatches[2]) + bdelta).toFixed(2) + bMatches[3];
            console.log(curBrightValue);
        } else {
            curBrightValue = "brightness(" + (1.0 + bdelta).toFixed(2) + ")";
        }
        el.style.filter = curBrightValue;
    }

    function seekBy(el, seekDelta){
        el.currentTime = el.currentTime + seekDelta;
    }

    function SeekButton(text, seekAmount, parent){
        let el = document.createElement("span");
        let self;

        el.innerHTML = text;
        el.style.marginRight = "10px";
        el.style.fontWeight = "bold";
        el.style.fontSize = BUTTON_SIZE;
        el.style.color = COLOR_NORMAL;
        el.style.cursor = "pointer";
        el.style.userSelect = "none";

        el.addEventListener("click", function(){
            seekBy(video_el, seekAmount);
        });

        parent.appendChild(el);

        return self = {
            el,
            text,
            seekAmount
        };
    }

    function BrightnessButton(text, brightness, parent){
        let el = document.createElement("span");
        let self;
        let curBrightness;

        el.innerHTML = text;
        el.style.marginRight = "10px";
        el.style.fontWeight = "bold";
        el.style.fontSize = BUTTON_SIZE;
        el.style.color = COLOR_NORMAL;
        el.style.cursor = "pointer";
        el.style.userSelect = "none";

        el.addEventListener("click", function(){
            if(text.indexOf("-") == -1 && text.indexOf("+") == -1){
                setBrightness(video_el, brightness);
                self.select();
            } else {
                changeBrightness(video_el, brightness);
            }
            displayCurrentBrightness();
        });

        parent.appendChild(el);

        function select(){
            if(brightButtons.last !== null){
                brightButtons.last.el.style.color = COLOR_NORMAL;
            }

            brightButtons.last = self;
            brightButtons.selected = self;
            el.style.color = COLOR_SELECTED;
        }

        function getprev(){
            if(self.prev === null){
                return self;
            }

            return brightButtons.selected = self.prev;
        }

        function getnext(){
            if(self.next === null){
                return self;
            }

            return brightButtons.selected = self.next;
        }

        return self = {
            el,
            text,
            brightness,
            curBrightness,
            prev:  null,
            next:  null,
            select,
            getprev,
            getnext
        };
    }

    function SpeedButton(text, speed, parent){
        let el = document.createElement("span");
        let self;

        el.innerHTML = text;
        el.style.marginRight = "10px";
        el.style.fontWeight = "bold";
        el.style.fontSize = BUTTON_SIZE;
        el.style.color = COLOR_NORMAL;
        el.style.cursor = "pointer";
        el.style.userSelect = "none";

        el.addEventListener("click", function(){
            setPlaybackRate(video_el, speed);
            self.select();
        });

        parent.appendChild(el);

        function select(){
            if(buttons.last !== null){
                buttons.last.el.style.color = COLOR_NORMAL;
            }

            buttons.last = self;
            buttons.selected = self;
            el.style.color = COLOR_SELECTED;
        }

        function getprev(){
            if(self.prev === null){
                return self;
            }

            return buttons.selected = self.prev;
        }

        function getnext(){
            if(self.next === null){
                return self;
            }

            return buttons.selected = self.next;
        }

        return self = {
            el,
            text,
            speed,
            prev:  null,
            next:  null,
            select,
            getprev,
            getnext
        };
    }

    function kill(){
        anchor.removeChild(speedContainer);
        anchor.removeChild(brightContainer);
        anchor.removeChild(seekContainer);
        //document.body.removeEventListener("keydown", ev_keyboard);
    }

    /*
    function ev_keyboard(ev){
        let match = keyboard_controls.find(([key, unused, callback]) => key === ev.key);
        let callback = (match || {2: ()=>null})[2];

        callback(ev);
    }*/

    setPlaybackRate(video_el, DEFAULT_SPEED);
    anchor.insertBefore(brightContainer.div, anchor.firstChild);
    anchor.insertBefore(speedContainer, anchor.firstChild);
    anchor.insertBefore(seekContainer, anchor.firstChild);
    //document.body.addEventListener("keydown", ev_keyboard);

    return {
        //controls: keyboard_controls,
        buttons,
        kill,
        SpeedButton,
        BrightnessButton,
        SeekButton,
        Infobox,
        setPlaybackRate,
        is_comment_box
    };
}

video_speed_buttons.from_query = function(anchor_q, video_q){
    return video_speed_buttons(
        document.querySelector(anchor_q),
        document.querySelector(video_q));
}

// Multi-purpose Loader (defaults to floating on top right)
const loader_data = {
    container_candidates: [
        // YouTube
        "div#container.ytd-video-primary-info-renderer",
        "div#watch-header",
        "div#watch7-headline",
        "div#watch-headline-title",
        // Vimeo
        ".clip_info-wrapper",
    ],

    css_div: [
        "position:    fixed",
        "top:         0",
        "right:       0",
        "zIndex:      100",
        "background:  rgba(0, 0, 0, 0.8)",
        "color:       #eeeeee",
        "padding:     10px"
    ].map(rule => rule.split(/: */)),

    css_vsb_container: [
        "borderBottom:    none",
        "marginBottom:    0",
        "paddingBottom:   0",
    ].map(rule => rule.split(/: */))
};

function logvsb(where, msg, lvl = 0){
    let logf = (["info", "error"])[lvl];

    console[logf](`[vsb::${where}] ${msg}`);
}

function loader_loop(){
    let vsbc = () => document.querySelector(".vsb-container");
    let vbbc = () => document.querySelector(".vbb-container");
    let vbsk = () => document.querySelector(".vbsk-container");
    let candidate;
    let default_candidate;

    if(vsbc() !== null){
        return;
    }

    candidate = loader_data
        .container_candidates
        .map(candidate => document.querySelector(candidate))
        .find(candidate => candidate !== null);

    default_candidate = (function(){
        let el = document.createElement("div");

        loader_data.css_div.forEach(function([name, value]){
            el.style[name] = value; });

        document.body.appendChild(el);
        return el;
    }());

    video_speed_buttons(candidate || default_candidate, document.querySelector("video"));

    if(candidate === null){
        logvsb("loader_loop", "no candidates for title section. Defaulting to top of page.");

        loader_data.css_vsb_container.forEach(function([name, value]){
            vsbc().style[name] = value;
        });
    }
}

setInterval(function(){
    if(document.readyState === "complete"){
        setTimeout(loader_loop, 2000);
    }
}, 2000); // Blame YouTube for this
